<?php
$connect=new Mongo();//Default http://localhost:27017
$db = $connect->selectDB( "Homework" );
$collection = $db->selectCollection( "Customers" );
$messageAlert = '';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customers</title>
 <link href="css/bootstrap.min.css" rel="stylesheet">
<style>
   
</style>
</head>
<body>

<?php
if(isset($_POST['bt_add'])){
 $name =  $_POST['name'];
 	if(!empty($name)){
 		$insert = array('name' => $name,'createdAt' => new DateTime('NOW'));
 		if($collection -> insert($insert)){
 			$messageAlert = '<center><font color="green">Success</font></center>';
 		}
 	}else{
 		$messageAlert = '<center><font color="green">Unsuccessful</font></center>';
 	}
}

if(isset($_POST['bt_edit'])){
 $name=$_POST['name2'];
 	if(!empty($name)){
 			$newdata = array('$set' => array('name' => $name,'createdAt' => new DateTime('NOW')));
 			if($collection -> update(array('_id' => new MongoId($_POST['editID'])),$newdata)){
			$messageAlert = '<center><font color="green">Success</font></center>';
 		}
 	}else{
 		$messageAlert = '<center><font color="green">Unsuccessful</font></center>';
 	}
}


if(isset($_GET['delID'])){
	if($collection->remove(array('_id' => new MongoId($_GET['delID'])))){
		$messageAlert = '<center><font color="green">Success</font></center>';
	}else{
		$messageAlert = '<center><font color="red">Unsuccessful</font>></center>';
	}
}

if(isset($_GET['delAll'])){
	if($collection->remove()){
	$messageAlert = '<center><font color="green">Success</font></center>';
}else{
	$messageAlert = '<center><font color="red">Unsuccessful</font>></center>';
} 
}

if(isset($_GET['q'])){
	$q=$_GET['q'];
	$regex=array('name' => new MongoRegex("/$q/i"));
	$showCate = $collection->find($regex);
}else{
	$showCate = $collection->find();
}
	$totalRec=$showCate->count();
?> 
<div class="container">
<div class="row">
	<table class="table">
		  <tr>
		   	<td align="center">
			<form id="searchForm" name="searchForm" method="get" action="">
			      	<label for="q"></label>
			      	ค้นหาข้อมูล
			 	<input type="text" name="q" id="q" />
			  	<input type="submit" name="bt_search" id="bt_search" value="ค้นหา" />
		    	</form></td>
		    	<td colspan="2" align="center">
			<form action="index.php" method="post">
			<!-- <a href="index.php">กลับหน้าหลัก/เพิ่มข้อมูล</a> <br /> -->
			จำนวน <?=$totalRec?> รายการ <br />
			<a href="?delAll=all" onclick="return confirm('ยืนยันการลบข้อมูลทั้งหมด');">ลบข้อมูลทั้งหมด</a>
			</form>
			</td>
		  </tr>
		  <tr>
		  	<td width="550" align="center" bgcolor="#FFCCCC"><strong>ชื่อประเภท</strong></td>
		   	<td colspan="2" align="center" bgcolor="#FFCCCC"><strong>จัดการ</strong></td>
		  </tr>
		  <?php
		if($totalRec>0){
			foreach($showCate as $id => $val){
		?>
				  <tr>
					<td><?=$val['name']?></td>
					<td width="99" align="center">
						<form action="index.php" method="post">
							<a href="?delID=<?=$id?>" onclick="return confirm('ยืนยันการลบข้อมูล');">[X]</a>
						</form>
					</td>
					<td width="79" align="center">
						<form action="index.php" method="post">
							<a href="?editID=<?=$id?>">แก้ไข
							</a>
						</form>
					</td>
				  </tr>
		  <?php } 
		}else{?>
		  <tr>
		  	<td colspan="3" align="center"><b>ไม่พบข้อมูล</b></td>
		  </tr>
		  <?php }?>
	</table>
</div>
<form action="index.php" method="post">
  <?php
	$editID =  isset($_GET['editID'])  ? $_GET['editID'] : '';
	if(empty($editID)){?>
	<div class="row">
	<div class="col-md-3"></div>
	 <div class="col-md-6">
	  <table class="table">
	    <tr>
	     	<td colspan="2" align="center" bgcolor="#00CCFF"><strong>ประเภทสินค้า</strong></td>
	    </tr>
	    <? if(isset($_POST['bt_add'])){?>
	    <? }?>
	    <tr>
	      	<td><strong>ชื่อประเภท</strong></td>
	      	<td><input type="text" name="name" id="name" /></td>
	    </tr>
	    <tr>
	      	<td> </td>
	      	<td><input type="submit" name="bt_add" id="bt_add" value="เพิ่มข้อมูล" /></td>
	    </tr>
	  </table>
	</div>
	<div class="col-md-3"></div>
	</div>
  <?php 
	}else{ 
		$showEdit = $collection->findOne(array('_id' => new MongoId($editID))); ?>
		<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-5">
	  <table class="table">
	    <tr>
	      	<td colspan="2" align="center" bgcolor="#99FF00"><strong>ประเภทสินค้า</strong></td>
	    </tr>
	    <tr>
	      	<td><strong>ชื่อประเภท</strong></td>
	      	<td><input type="text" name="name2" id="name2" value="<?=$showEdit['name']?>" /></td>
	    </tr>
	    <tr>
	      	<td> </td>
	      	<td><input type="submit" name="bt_edit" id="bt_edit" value="แก้ไข" />
	        	<input type="hidden" name="editID" id="editID" value="<?=$showEdit['_id']?>" /></td>
	    </tr>
	  </table>
	</div>
	<div class="col-md-3"></div>
	</div>
  <?php }?>

<div class="row">
	<div class="col-md-3"></div>
	 <div class="col-md-6">
	 	<?php echo $messageAlert; ?>
	</div>
	<div class="col-md-3"></div>
	</div>
</form>

</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>


 
<?php $connect->close();?>
